package fr.d4.demo.util;

import java.util.Date;

import javax.crypto.SecretKey;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwt;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

public class JWTUtils {
	private static SecretKey SECRET_KEY = Keys.secretKeyFor(SignatureAlgorithm.HS256);
	private JWTUtils() {
		super();
	}
	
	/**
	 * Permet de generer un token
	 * @param issuer : emetteur
	 * @param audience : destinataire(s)
	 * @param subject : sujet
	 * @param role : role de la personne qui s'authentifie (ajoutee)
	 * @param duree : duree
	 * @return le token genere a partir des parametres et du secret
	 */
	public static String createJWT(String issuer, String audience, String subject, String role, long duree) {
		return Jwts.builder()
				.setIssuer(issuer)
				.setAudience(audience)
				.setSubject(subject)
				.claim("role", role)
				.setIssuedAt(new Date())
				.setExpiration(new Date(System.currentTimeMillis() + duree))
				.signWith(SECRET_KEY)
				.compact();
	}
	
	/**
	 * Permet de decoder un token
	 * @param token : le token a decoder
	 * @return un objet JWT si le token est correct et null sinon
	 */
	public static Jwt decodeJWT(String token) {
		try {
			return Jwts.parserBuilder()
					.setSigningKey(SECRET_KEY)
					.build()
					.parse(token);
		} catch (Exception e) {
			return null;
		}
	}
	
	
	/**
	 * Permet de verifier si la personne qui s'authentifie possede un token valide
	 * puis verifie son role
	 * @param token : le token de la personne qui s'authentifie
	 * @param role : le role que l'on souhaite verifier
	 * @return true si l'authentification est valide et false sinon
	 */
	public static boolean isAuthentifie(String token, String role) {
		Jwt tokenDecode = decodeJWT(token);
		if (tokenDecode != null) {
			Claims claim = (Claims) tokenDecode.getBody();
			return role.equals(claim.get("role"));
		}
		return false;
	}
	
}
