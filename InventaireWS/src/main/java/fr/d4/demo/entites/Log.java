package fr.d4.demo.entites;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class Log {
	private String login;
	private String motdepasse;
	private Personne personne;
	
	public Log(String login, String motdepasse) {
		this.login = login;
		this.motdepasse = motdepasse;
	}
	public Log(String login, String motdepasse, Personne personne) {
		super();
		this.login = login;
		this.motdepasse = motdepasse;
		this.personne = personne;
	}
	public Log() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
