package fr.d4.demo.entites;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class Objet {
	private Long id_objet;
	private String nom;
	private String release;
	private List<Offre> listeOffre;
	private TypeObjet typeObjet;
	private List<Personne> listePersonne;
	private List<Langue> listeLangueParler;
	private List<Oeuvre> listeOeuvre;
	
	
	public Objet() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Objet(Long id_objet, String nom, String release) {
		super();
		this.id_objet = id_objet;
		this.nom = nom;
		this.release = release;
	}



	
	
	
}
