package fr.d4.demo.entites;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class Commentaire {
	private Long id_com;
	private Double note;
	private String corps;
	private String expediteur;
	private Personne personne;
	public Commentaire() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Commentaire(Long id_com, Double note, String corps, String expediteur, Personne personne) {
		super();
		this.id_com = id_com;
		this.note = note;
		this.corps = corps;
		this.expediteur = expediteur;
		this.personne = personne;
	}
	
	
}
