package fr.d4.demo.entites;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@EqualsAndHashCode
public class CreerOeuvre {
	private String nom;
	private String release;
	private String typeOeuvre;
	List<Createur> listeCreateur;

}
