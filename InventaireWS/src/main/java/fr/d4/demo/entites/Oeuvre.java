package fr.d4.demo.entites;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class Oeuvre {
	private Long id_oeuvre;
	private String nom;
	private String release;
	private String typeOeuvre;
	private List<String> listeCreateur;
	public Oeuvre() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Oeuvre(Long id_oeuvre, String nom, String release, String typeOeuvre, List<String> listeCreateur) {
		super();
		this.id_oeuvre = id_oeuvre;
		this.nom = nom;
		this.release = release;
		this.typeOeuvre = typeOeuvre;
		this.listeCreateur = listeCreateur;
	}
	
	
}
