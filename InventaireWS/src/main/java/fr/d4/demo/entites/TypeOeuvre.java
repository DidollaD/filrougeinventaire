package fr.d4.demo.entites;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class TypeOeuvre {
	private Long id_typeoeuvre;
	private String libelle;
	private List<Oeuvre> listeOeuvre;
	public TypeOeuvre() {
		super();
		// TODO Auto-generated constructor stub
	}
	public TypeOeuvre(Long id_typeoeuvre, String libelle, List<Oeuvre> listeOeuvre) {
		super();
		this.id_typeoeuvre = id_typeoeuvre;
		this.libelle = libelle;
		this.listeOeuvre = listeOeuvre;
	}
	
	
}
