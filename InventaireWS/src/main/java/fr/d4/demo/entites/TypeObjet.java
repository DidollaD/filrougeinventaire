package fr.d4.demo.entites;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class TypeObjet {
	private Long id_typeobjet;
	private String libelle;
	private String support;
	private List<Objet> listeObjet;
	public TypeObjet() {
		super();
		// TODO Auto-generated constructor stub
	}
	public TypeObjet(Long id_typeobjet, String libelle, String support, List<Objet> listeObjet) {
		super();
		this.id_typeobjet = id_typeobjet;
		this.libelle = libelle;
		this.support = support;
		this.listeObjet = listeObjet;
	}
	
	
}
