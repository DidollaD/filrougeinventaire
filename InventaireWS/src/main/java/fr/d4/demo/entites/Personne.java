package fr.d4.demo.entites;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class Personne {
	private Long id_personne;
	private String login;
	private String email;
	private String nomComplet;
	private boolean admin;
	private boolean actif;
	private Log log;
	private List<Offre> listeOffre;
	private List<Commentaire> listeCom;
	private List<Objet> listeObjet;
	public Personne( String login, String email, String nomComplet, boolean admin, boolean actif) {
		this.login = login;
		this.email = email;
		this.nomComplet = nomComplet;
		this.admin = admin;
		this.actif = actif;
	}
	public Personne(Long id_personne, String login, String email, String nomComplet, boolean admin, boolean actif,
			Log log, List<Offre> listeOffre, List<Commentaire> listeCom, List<Objet> listeObjet) {
		super();
		this.id_personne = id_personne;
		this.login = login;
		this.email = email;
		this.nomComplet = nomComplet;
		this.admin = admin;
		this.actif = actif;
		this.log = log;
		this.listeOffre = listeOffre;
		this.listeCom = listeCom;
		this.listeObjet = listeObjet;
	}
	public Personne() {
		// TODO Auto-generated constructor stub
	}

	
}
