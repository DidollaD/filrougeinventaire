package fr.d4.demo.entites;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class Langue {
	private Long id_langue;
	private String libelle;
	private List<Objet> listeObjetParler;
	private List<Objet> listeObjetEcrit;
	public Langue() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Langue(Long id_langue, String libelle, List<Objet> listeObjetParler, List<Objet> listeObjetEcrit) {
		super();
		this.id_langue = id_langue;
		this.libelle = libelle;
		this.listeObjetParler = listeObjetParler;
		this.listeObjetEcrit = listeObjetEcrit;
	}
	
	
}
