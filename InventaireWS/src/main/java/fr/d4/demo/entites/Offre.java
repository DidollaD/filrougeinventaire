package fr.d4.demo.entites;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class Offre {
	private Long id_offre;
	private Double prix;
	private String image;
	private String description;
	private Personne personne;
	private Objet objet;
	public Offre() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Offre(Long id_offre, Double prix, String image, String description, Personne personne, Objet objet) {
		super();
		this.id_offre = id_offre;
		this.prix = prix;
		this.image = image;
		this.description = description;
		this.personne = personne;
		this.objet = objet;
	}
	
	
}
