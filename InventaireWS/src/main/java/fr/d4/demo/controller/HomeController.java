package fr.d4.demo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import fr.d4.demo.IControl.IControlAuth;
import fr.d4.demo.IServices.IServiceCreation;
import fr.d4.demo.IServices.IServiceModif;
import fr.d4.demo.IServices.IServicePersonne;
import fr.d4.demo.IServices.IServiceVoir;
import fr.d4.demo.entites.CreerOeuvre;
import fr.d4.demo.entites.CreerPersonne;
import fr.d4.demo.entites.Log;
import fr.d4.demo.entites.Objet;
import fr.d4.demo.entites.Oeuvre;
import fr.d4.demo.entites.Personne;
import fr.d4.demo.entites.ReponseAuth;
import fr.d4.demo.entitescom.StatusCreation;
import fr.d4.demo.entitescom.StatusObjetReturn;
import fr.d4.demo.util.JWTUtils;



@CrossOrigin(value = "http://localhost:3000")
@RestController
public class HomeController {

	@Autowired
	private IServiceVoir serviceVoir;
	
	@Autowired
	private IServiceCreation serviceCreation;
	
	@Autowired
	private IServiceModif serviceModif;
	
	@Autowired
	private IServicePersonne servicePersonne;
	
	@Autowired
	private IControlAuth controleAuth;
	/**
	 * ce Controlleur permet de voir la liste des personne
	 * 
	 * @return la liste des personnes
	 */
	@GetMapping(value = "/VLU")
	@ResponseBody
	public StatusObjetReturn voirPersonne() {
		StatusObjetReturn reponse = new StatusObjetReturn();

		Map<Long, Personne> temp = serviceVoir.allPersonnes();
		List<Personne> listePersonnes = new ArrayList<Personne>();
		for (Entry<Long, Personne> personne : temp.entrySet()) {
			listePersonnes.add(personne.getValue());
		}
		reponse.setObjetReturn(listePersonnes);
		return reponse;
	}
	
	/**
	 * ce Controlleur permet de voir la liste des Oeuvre
	 * 
	 * @return la liste des personnes
	 */
	@GetMapping(value = "/VLOe")
	@ResponseBody
	public StatusObjetReturn voirOeuvre() {
		StatusObjetReturn reponse = new StatusObjetReturn();

		Map<Long, Oeuvre> temp = serviceVoir.allOeuvre();
		List<Oeuvre> listeOeuvres = new ArrayList<Oeuvre>();
		for (Entry<Long, Oeuvre> oeuvre : temp.entrySet()) {
			listeOeuvres.add(oeuvre.getValue());
		}
		reponse.setObjetReturn(listeOeuvres);
		return reponse;
	}
	
	/**
	 * ce Controlleur permet de voir la liste des Oeuvre
	 * 
	 * @return la liste des personnes
	 */
	@GetMapping(value = "/VLO")
	@ResponseBody
	public StatusObjetReturn voirObjet() {
		StatusObjetReturn reponse = new StatusObjetReturn();

		Map<Long, Objet> temp = serviceVoir.allObjet();
		List<Objet> listeObjets = new ArrayList<Objet>();
		for (Entry<Long, Objet> oeuvre : temp.entrySet()) {
			listeObjets.add(oeuvre.getValue());
		}
		reponse.setObjetReturn(listeObjets);
		return reponse;
	}
	
	
	/**
	 * Controller permettant de logguer la personne en fonction de son type de
	 * profil, admin ou utilisateur
	 * 
	 */
	@PostMapping(value = "/Auth")
	@ResponseBody
	public ReponseAuth authentificationRest(@RequestBody Log auth) {
		String token = null;
		if (controleAuth.ValideAuth(auth.getLogin(),
				auth.getMotdepasse())) {
			Personne personne = servicePersonne.TrouvePersonne(auth.getLogin(), auth.getMotdepasse());
			if (!personne.isAdmin() && personne.isActif()) {
				token = JWTUtils.createJWT(auth.getLogin(), "gestionsalleAPI", "connexion", "utilisateur", 10000000000L);
				return new ReponseAuth(0, token);
			} 
			else if (personne.isAdmin()) {
				token = JWTUtils.createJWT(auth.getLogin(), "gestionsalleAPI", "connexion", "administrateur", 10000000000L);
				return new ReponseAuth(1, token);
			}
		} 
		return new ReponseAuth(-1, token);
	}
	
	@PostMapping(value = "/CUPA")
	@ResponseBody
	public StatusCreation CreationPersonneAdmin(@RequestHeader HttpHeaders header, @RequestBody CreerPersonne creerPersonne) {
		StatusCreation reponse = new StatusCreation();
		int coderetour = 0;
		if ("undefined".equals(creerPersonne.getNom()) 
				|| "undefined".equals(creerPersonne.getMail()) 
				|| "undefined".equals(creerPersonne.getLogin()) || "undefined".equals(creerPersonne.getPassword())
				|| "undefined".equals(creerPersonne.getPassword2())) {
			reponse.setCodeRetour(-2);
			
		}
		if (!creerPersonne.getNom().matches(
				"[\\w ]{3,50}$")) {
			coderetour = -3;
		}
		if (!creerPersonne.getLogin().matches(
				"[\\w ]{3,15}$")) {
			coderetour = -3;
		}
		if (!creerPersonne.getMail().matches(
				"[\\w!#$%&�*+/=?`{|}~^-]+(?:\\.[\\w!#$%&�*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$")) {
			coderetour = -5;
		}
		if (!creerPersonne.getPassword().matches("\\w{8,50}")) {
			coderetour = -6;
		}
		if (!creerPersonne.getPassword().equals(creerPersonne.getPassword2())) {
			coderetour = -8;
		}
		if(coderetour == 0) {
			serviceCreation.CreationPersonneAdmin(creerPersonne.getNom(),creerPersonne.getMail(),creerPersonne.getLogin(),creerPersonne.getPassword());
		}
		return reponse;
	}
	
	@PostMapping(value = "/CUP")
	@ResponseBody
	public StatusCreation CreationPersonne(@RequestHeader HttpHeaders header, @RequestBody CreerPersonne creerPersonne) {
		StatusCreation reponse = new StatusCreation();
		int coderetour = 0;
		if ("undefined".equals(creerPersonne.getNom()) 
				|| "undefined".equals(creerPersonne.getMail()) 
				|| "undefined".equals(creerPersonne.getLogin()) || "undefined".equals(creerPersonne.getPassword())
				|| "undefined".equals(creerPersonne.getPassword2())) {
			reponse.setCodeRetour(-2);
			
		}
		if (!creerPersonne.getNom().matches(
				"[\\w ]{3,50}$")) {
			coderetour = -3;
		}
		if (!creerPersonne.getLogin().matches(
				"[\\w ]{3,15}$")) {
			coderetour = -3;
		}
		if (!creerPersonne.getMail().matches(
				"[\\w!#$%&�*+/=?`{|}~^-]+(?:\\.[\\w!#$%&�*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$")) {
			coderetour = -5;
		}
		if (!creerPersonne.getPassword().matches("\\w{8,50}")) {
			coderetour = -6;
		}
		if (!creerPersonne.getPassword().equals(creerPersonne.getPassword2())) {
			coderetour = -8;
		}
		if(coderetour == 0) {
			serviceCreation.CreationPersonne(creerPersonne.getNom(),creerPersonne.getMail(),creerPersonne.getLogin(),creerPersonne.getPassword());
		}
		return reponse;
	}
	
	@PostMapping(value = "/CUO")
	@ResponseBody
	public StatusCreation CreationOeuvre(@RequestHeader HttpHeaders header, @RequestBody CreerOeuvre creerOeuvre) {
		StatusCreation reponse = new StatusCreation();
		int coderetour = 0;
		if ("undefined".equals(creerOeuvre.getNom()) 
				|| "undefined".equals(creerOeuvre.getRelease())) {
			reponse.setCodeRetour(-2);
		}
		if (!creerOeuvre.getNom().matches(
				"[\\w ]{1,50}$")) {
			coderetour = -3;
		}
		if (!creerOeuvre.getRelease().matches(
				"[\\d]{4}$")) {
			coderetour = -4;
		}
		if(coderetour == 0) {
//			serviceCreation.CreationOeuvre(creerOeuvre.getNom(),creerOeuvre.getRelease());
		}
		return reponse;		
	}
	
	
	
	@DeleteMapping(value = "/SMURest")
	public StatusObjetReturn suppressionUtilisateurRest(@RequestHeader HttpHeaders header, @RequestParam(value = "id_personne") Long id) {
		StatusObjetReturn reponse = new StatusObjetReturn();
		reponse.setObjetRetourne(serviceModif.supprimerPersonne(id));
		return reponse;
	}
}
