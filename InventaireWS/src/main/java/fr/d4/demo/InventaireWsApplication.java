package fr.d4.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InventaireWsApplication {

	public static void main(String[] args) {
		SpringApplication.run(InventaireWsApplication.class, args);
	}

}
