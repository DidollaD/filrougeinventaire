package fr.d4.demo.control;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.d4.demo.IControl.IControlAuth;
import fr.d4.demo.IDto.IDtoPersonne;

@Service
public class ControlAuth implements IControlAuth {
	
	@Autowired
	private IDtoPersonne dtoPersonne;
	
	/**
	 * Permet de verifier si une personne est inscrite sur le site
	 * 
	 * @param login : le login a verifier
	 * @param mdp   : le mot de passe a verifier
	 * @return true si la personne est inscrite et false sinon
	 */
	@Override
	public boolean ValideAuth(String log, String mdp) {
		return dtoPersonne.ValideAuth(log, mdp);
	}
	
}
