package fr.d4.demo.IDto;

import java.util.Map;

import org.springframework.stereotype.Service;

import fr.d4.demo.entites.Log;
import fr.d4.demo.entites.Personne;
@Service
public interface IDtoPersonne {
	public Map<Long, Personne> allPersonnes() ;
	
	public boolean ValideAuth(String log, String mdp);
	
	public Personne TrouvePersonne(String log , String mdp) ;
	
	public void CreationPersonne(Log log, Personne personne);

	public boolean supprimer(Long id);
}
