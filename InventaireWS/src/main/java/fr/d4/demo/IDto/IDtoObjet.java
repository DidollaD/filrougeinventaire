package fr.d4.demo.IDto;

import java.util.Map;

import org.springframework.stereotype.Service;

import fr.d4.demo.entites.Objet;
@Service
public interface IDtoObjet {

	Map<Long, Objet> allObjet();

}
