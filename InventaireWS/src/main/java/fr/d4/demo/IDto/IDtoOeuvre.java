package fr.d4.demo.IDto;

import java.util.Map;

import org.springframework.stereotype.Service;

import fr.d4.demo.entites.Oeuvre;
@Service
public interface IDtoOeuvre {

	Map<Long, Oeuvre> allOeuvre();

}
