package fr.d4.demo.IDto;

import org.springframework.stereotype.Service;

import fr.d4.demo.entites.Log;
import fr.d4.demo.entites.Objet;
import fr.d4.demo.entites.Oeuvre;
import fr.d4.demo.entites.Personne;
import fr.d4.demo.model.LogDao;
import fr.d4.demo.model.ObjetDao;
import fr.d4.demo.model.OeuvreDao;
import fr.d4.demo.model.PersonneDao;

@Service
public interface IDtoGeneral {
	public Personne PersonneDaoToPersonne(PersonneDao personneDao);
	
	public PersonneDao PersonneToPersonneDao(Personne personne);

	public LogDao LogToLogDao(Log log);

	public Oeuvre OeuvreDaoToOeuvre(OeuvreDao oeuvreDao);

	public Objet ObjetDaoToObjet(ObjetDao oeuvreDao);
}
