package fr.d4.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.d4.demo.IDto.IDtoPersonne;
import fr.d4.demo.IServices.IServicePersonne;
import fr.d4.demo.entites.Personne;

@Service
public class ServicePersonne implements IServicePersonne {

	@Autowired
	IDtoPersonne dtoPersonne;

	@Override
	public Personne TrouvePersonne(String log, String mdp) {
		return dtoPersonne.TrouvePersonne(log, mdp);

	}

}
