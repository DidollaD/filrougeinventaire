package fr.d4.demo.service;

import org.springframework.stereotype.Service;

import fr.d4.demo.IDto.IDtoPersonne;
import fr.d4.demo.IServices.IServiceModif;
@Service
public class ServiceModif implements IServiceModif {

	private IDtoPersonne dtoPersonne;
	
	@Override
	public boolean supprimerPersonne(Long id) {
		return dtoPersonne.supprimer(id);
	}

}
