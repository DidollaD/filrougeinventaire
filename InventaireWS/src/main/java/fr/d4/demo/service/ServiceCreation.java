package fr.d4.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.d4.demo.IDto.IDtoPersonne;
import fr.d4.demo.IServices.IServiceCreation;
import fr.d4.demo.entites.Log;
import fr.d4.demo.entites.Personne;

@Service
public class ServiceCreation implements IServiceCreation {

	@Autowired
	IDtoPersonne dtoPersonne;
	
	@Override
	public void CreationPersonne(String nom, String mail, String login, String password) {
		Log log = new Log(login, password);
		Personne personne = new Personne(login, mail, nom, false, true);
		dtoPersonne.CreationPersonne(log, personne);
	}

	@Override
	public void CreationPersonneAdmin(String nom, String mail, String login, String password) {
		Log log = new Log(login, password);
		Personne personne = new Personne(login, mail, nom, true, true);
		dtoPersonne.CreationPersonne(log, personne);
	}

}
