package fr.d4.demo.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import fr.d4.demo.IDto.IDtoObjet;
import fr.d4.demo.IDto.IDtoOeuvre;
import fr.d4.demo.IDto.IDtoPersonne;
import fr.d4.demo.IServices.IServiceVoir;
import fr.d4.demo.entites.Objet;
import fr.d4.demo.entites.Oeuvre;
import fr.d4.demo.entites.Personne;

@Service
@Primary
public class ServiceVoir implements IServiceVoir{
	
	@Autowired
	private IDtoPersonne dtoPersonne;
	
	@Autowired
	private IDtoOeuvre dtoOeuvre; 
	
	@Autowired
	private IDtoObjet dtoObjet; 
	/**
	 * Permet de retourner la liste de toutes les personnes
	 * 
	 * @return la liste de toutes les personnes
	 */
	public Map<Long, Personne> allPersonnes() {
		return dtoPersonne.allPersonnes();
	}
	@Override
	public Map<Long, Oeuvre> allOeuvre() {
		return dtoOeuvre.allOeuvre();
	}
	@Override
	public Map<Long, Objet> allObjet() {
		
		return dtoObjet.allObjet();
	}
}
