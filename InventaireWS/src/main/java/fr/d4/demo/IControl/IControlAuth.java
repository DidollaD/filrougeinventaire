package fr.d4.demo.IControl;

import org.springframework.stereotype.Service;

@Service
public interface IControlAuth {
	public boolean ValideAuth(String log, String mdp);
}
