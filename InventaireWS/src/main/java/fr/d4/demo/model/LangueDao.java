package fr.d4.demo.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

@Data
@Entity(name = "langue")
public class LangueDao {
	@SequenceGenerator(name = "LANGUE_SEQ", initialValue = 2, allocationSize = 1)
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LANGUE_SEQ")
	private Long id_langue;
	@Column
	private String libelle;
	
	@ManyToMany
	private List<ObjetDao> listeObjetParler;
	
	@ManyToMany
	private List<ObjetDao> listeObjetEcrit;
}
