package fr.d4.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

@Data
@Entity(name = "offre")
public class OffreDao {
	@SequenceGenerator(name = "OFFRE_SEQ", initialValue = 2, allocationSize = 1)
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OFFRE_SEQ")
	private Long id_offre;
	@Column
	private Double prix;
	@Column
	private String image;
	@Column
	private String description;
	
	@ManyToOne
	@JoinColumn(name = "id_personne")
	private PersonneDao persoff;
	
	@ManyToOne
	@JoinColumn(name = "id_objet")
	private ObjetDao objet;
}
