package fr.d4.demo.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

@Data
@Entity(name = "archive")
public class ArchiveDao {
	@SequenceGenerator(name = "ARCHIVE_SEQ", initialValue = 1, allocationSize = 1)

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ARCHIVE_SEQ")
	private Long id_archive;
	@Column
	private String email;
	@Column(name = "nomcomplet")
	private String nomComplet;
	@Column
	private boolean admin;
	@Column
	private boolean actif;
	@Column
	private String login;
}
