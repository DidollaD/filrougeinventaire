package fr.d4.demo.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter

@Data
@Entity(name = "oeuvre")
public class OeuvreDao {
	
	@SequenceGenerator(name = "OEUVRE_SEQ", initialValue = 2, allocationSize = 1)
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OEUVRE_SEQ")
	private Long id_oeuvre;
	@Column
	private String nom;
	@Column
	private String release;
	
	@ManyToOne
	@JoinColumn(name = "id_typeoeuvre")
	private TypeOeuvreDao typeOeuvre;
	
	
	@ManyToMany
	private List<CreateurDao> listeCreateur;
	
}
