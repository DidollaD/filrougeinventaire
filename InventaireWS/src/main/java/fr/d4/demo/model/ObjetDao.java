package fr.d4.demo.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

@Data
@Entity(name = "objet")
public class ObjetDao {
	@SequenceGenerator(name = "OBJ_SEQ", initialValue = 2, allocationSize = 1)

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OBJ_SEQ")
	private Long id_objet;
	@Column
	private String nom;
	@Column
	private String release;
	
	@OneToMany(mappedBy = "objet")
	private List<OffreDao> listeOffre;
	
	@ManyToOne
	@JoinColumn(name = "id_typeobjet")
	private TypeObjetDao typeObjet;
	
	@ManyToMany
	private List<PersonneDao> listePersonne;
	
	@ManyToMany
	private List<LangueDao> listeLangueParler;
	
	@ManyToMany
	private List<OeuvreDao> listeOeuvre;
}
