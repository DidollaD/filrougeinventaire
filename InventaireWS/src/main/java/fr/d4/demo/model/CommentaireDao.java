package fr.d4.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

@Data
@Entity(name = "com")
public class CommentaireDao {
	@SequenceGenerator(name = "COM_SEQ", initialValue = 2, allocationSize = 1)
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COM_SEQ")
	private Long id_com;
	@Column
	private Double note;
	@Column
	private String corps;
	@Column
	private String expediteur;
	
	@ManyToOne
	@JoinColumn(name = "id_personne")
	private PersonneDao perscom;
	
}
