package fr.d4.demo.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

@Data
@Entity(name = "typeobjet")
public class TypeObjetDao {
	@SequenceGenerator(name = "TYPEOBJET_SEQ", initialValue = 2, allocationSize = 1)
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TYPEOBJET_SEQ")
	private Long id_typeobjet;
	@Column
	private String libelle;
	@Column
	private String support;
	
	@OneToMany(mappedBy = "typeObjet")
	private List<ObjetDao> listeObjet;

}
