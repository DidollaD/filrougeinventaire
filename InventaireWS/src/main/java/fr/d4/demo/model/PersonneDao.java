package fr.d4.demo.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

@Data
@Entity(name = "personne")
public class PersonneDao {
	
	@SequenceGenerator(name = "PROFIL_SEQ", initialValue = 2, allocationSize = 1)
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PROFIL_SEQ")
	private Long id_personne;
	@Column
	private String login;
	@Column
	private String email;
	@Column(name = "nomcomplet")
	private String nomComplet;
	@Column
	private boolean admin;
	@Column
	private boolean actif;
	
	@OneToOne(mappedBy = "perslog", cascade = CascadeType.ALL)
	private LogDao log;
	
	@OneToMany(mappedBy = "persoff")
	private List<OffreDao> listeOffre;
	
	@OneToMany(mappedBy = "perscom")
	private List<CommentaireDao> listeCom;
	
	@ManyToMany
	private List<ObjetDao> listeObjet;

	

	public PersonneDao() {
		super();
	}
	
	public PersonneDao(Long id_personne, String email, String nomComplet, boolean admin) {
		super();
		this.id_personne = id_personne;
		this.email = email;
		this.nomComplet = nomComplet;
		this.admin = admin;
		this.actif = true;
	}



	
	
	
}
