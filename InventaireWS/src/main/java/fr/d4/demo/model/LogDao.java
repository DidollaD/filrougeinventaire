package fr.d4.demo.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter

@Data
@Entity(name = "login")
public class LogDao {

	@Id
	@OnDelete(action = OnDeleteAction.CASCADE)
	private String login;
	@Column
	private String motdepasse;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_personne")
	private PersonneDao perslog;

	public LogDao() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LogDao(String login, String motdepasse) {
		super();
		this.login = login;
		this.motdepasse = motdepasse;
	}

	
	
	

}
