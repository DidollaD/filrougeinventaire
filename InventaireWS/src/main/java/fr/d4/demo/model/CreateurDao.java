package fr.d4.demo.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

@Data
@Entity(name = "createur")
public class CreateurDao {
	@SequenceGenerator(name = "CREA_SEQ", initialValue = 2, allocationSize = 1)
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CREA_SEQ")
	private Long id_crea;
	@Column
	private String nom;
	@Column
	private String libelle;
	@ManyToMany
	private List<OeuvreDao> listeOeuvre;
}
