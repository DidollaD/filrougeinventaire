package fr.d4.demo.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.d4.demo.model.TypeObjetDao;

public interface ITypeObjetRepository extends JpaRepository<TypeObjetDao, Long> {

}
