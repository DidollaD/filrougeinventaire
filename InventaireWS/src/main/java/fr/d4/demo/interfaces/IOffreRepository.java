package fr.d4.demo.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.d4.demo.model.OffreDao;

public interface IOffreRepository extends JpaRepository<OffreDao, Long> {

}
