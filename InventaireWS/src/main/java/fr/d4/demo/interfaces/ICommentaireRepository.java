package fr.d4.demo.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.d4.demo.model.CommentaireDao;

public interface ICommentaireRepository extends JpaRepository<CommentaireDao, Long> {

}
