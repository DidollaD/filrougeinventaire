package fr.d4.demo.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.d4.demo.model.ArchiveDao;

public interface IArchiveRepository extends JpaRepository<ArchiveDao, Long> {

}
