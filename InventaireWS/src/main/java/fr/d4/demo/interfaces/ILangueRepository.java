package fr.d4.demo.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.d4.demo.model.LangueDao;

public interface ILangueRepository extends JpaRepository<LangueDao, Long> {

}
