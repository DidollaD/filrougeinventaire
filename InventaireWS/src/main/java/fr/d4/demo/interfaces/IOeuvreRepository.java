package fr.d4.demo.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.d4.demo.model.OeuvreDao;

public interface IOeuvreRepository extends JpaRepository<OeuvreDao, Long> {

}
