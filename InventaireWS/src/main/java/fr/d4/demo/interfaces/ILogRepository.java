package fr.d4.demo.interfaces;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.d4.demo.model.LogDao;

public interface ILogRepository extends JpaRepository<LogDao, Long> {
	List<LogDao> findByLoginAndMotdepasse(String login, String mdp);
}
