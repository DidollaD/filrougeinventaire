package fr.d4.demo.interfaces;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.d4.demo.model.TypeOeuvreDao;

public interface ITypeOeuvreRepository extends JpaRepository<TypeOeuvreDao, Long> {

}
