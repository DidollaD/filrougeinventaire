package fr.d4.demo.interfaces;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.d4.demo.model.PersonneDao;

@Repository
public interface IPersonneRepository extends JpaRepository<PersonneDao, Long>{

	PersonneDao findByLogin(String log);

	

}
