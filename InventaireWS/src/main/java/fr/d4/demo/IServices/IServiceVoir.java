package fr.d4.demo.IServices;

import java.util.Map;

import org.springframework.stereotype.Service;

import fr.d4.demo.entites.Objet;
import fr.d4.demo.entites.Oeuvre;
import fr.d4.demo.entites.Personne;

@Service
public interface IServiceVoir {
	
	/**
	 * Permet de retourner la liste de toutes les personnes
	 * 
	 * @return la liste de toutes les personnes
	 */
	public Map<Long, Personne> allPersonnes() ;

	public Map<Long, Oeuvre> allOeuvre();

	public Map<Long, Objet> allObjet();
}
