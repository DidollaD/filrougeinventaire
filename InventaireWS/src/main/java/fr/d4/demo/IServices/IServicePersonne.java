package fr.d4.demo.IServices;

import org.springframework.stereotype.Service;

import fr.d4.demo.entites.Personne;

@Service
public interface IServicePersonne {
	public Personne TrouvePersonne(String log , String mdp) ;
}
