package fr.d4.demo.IServices;

import org.springframework.stereotype.Service;

@Service
public interface IServiceModif {

	public boolean supprimerPersonne(Long id);

}
