package fr.d4.demo.IServices;

import org.springframework.stereotype.Service;

@Service
public interface IServiceCreation {
	public void CreationPersonne(String nom,String mail ,String login,String password);

	public void CreationPersonneAdmin(String nom, String mail, String login, String password);
}
