package fr.d4.demo.dto;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.d4.demo.IDto.IDtoGeneral;
import fr.d4.demo.IDto.IDtoOeuvre;
import fr.d4.demo.entites.Oeuvre;
import fr.d4.demo.interfaces.IOeuvreRepository;
import fr.d4.demo.model.OeuvreDao;
@Service
public class DtoOeuvre implements IDtoOeuvre {
	
	@Autowired
	private IOeuvreRepository oeuvreRepository;
	@Autowired
	private IDtoGeneral dtoGeneral;

	@Override
	public Map<Long, Oeuvre> allOeuvre() {
		Map<Long, Oeuvre> listeOeuvre = new HashMap<Long, Oeuvre>();
		for (OeuvreDao oeuvreDao : oeuvreRepository.findAll()) {
			listeOeuvre.put(oeuvreDao.getId_oeuvre(), dtoGeneral.OeuvreDaoToOeuvre(oeuvreDao));
		}
		return listeOeuvre;
		
	}

}
