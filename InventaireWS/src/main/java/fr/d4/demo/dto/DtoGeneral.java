package fr.d4.demo.dto;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import fr.d4.demo.IDto.IDtoGeneral;
import fr.d4.demo.entites.Log;
import fr.d4.demo.entites.Objet;
import fr.d4.demo.entites.Oeuvre;
import fr.d4.demo.entites.Personne;
import fr.d4.demo.model.CreateurDao;
import fr.d4.demo.model.LogDao;
import fr.d4.demo.model.ObjetDao;
import fr.d4.demo.model.OeuvreDao;
import fr.d4.demo.model.PersonneDao;

@Service
public class DtoGeneral implements IDtoGeneral {

	@Override
	public Personne PersonneDaoToPersonne(PersonneDao personneDao) {
		Personne personne = new Personne();
		personne.setId_personne(personneDao.getId_personne());
		personne.setActif(personneDao.isActif());
		personne.setAdmin(personneDao.isAdmin());
		personne.setEmail(personneDao.getEmail());
		personne.setLogin(personneDao.getLogin());
		personne.setNomComplet(personneDao.getNomComplet());
		return personne;
	}
	
	@Override
	public PersonneDao PersonneToPersonneDao(Personne personne) {
		PersonneDao personneDao = new PersonneDao();
		personneDao.setId_personne(personne.getId_personne());
		personneDao.setActif(personne.isActif());
		personneDao.setAdmin(personne.isAdmin());
		personneDao.setEmail(personne.getEmail());
		personneDao.setLogin(personne.getLogin());
		personneDao.setNomComplet(personne.getNomComplet());
		return personneDao;
	}
	@Override
	public LogDao LogToLogDao(Log log) {
		LogDao logDao = new LogDao();
		logDao.setLogin(log.getLogin());
		logDao.setMotdepasse(log.getMotdepasse());
		return logDao;
	}

	
	
	@Override
	public Oeuvre OeuvreDaoToOeuvre(OeuvreDao oeuvreDao) {
		Oeuvre oeuvre = new Oeuvre();
		oeuvre.setId_oeuvre(oeuvreDao.getId_oeuvre());
		oeuvre.setNom(oeuvreDao.getNom());
		oeuvre.setRelease(oeuvreDao.getRelease());
		oeuvre.setTypeOeuvre(oeuvreDao.getTypeOeuvre().getLibelle());
		oeuvre.setListeCreateur(new ArrayList<String>());
		for (CreateurDao createur  : oeuvreDao.getListeCreateur()) {
			oeuvre.getListeCreateur().add(createur.getNom());
		}
		return oeuvre;
	}

	@Override
	public Objet ObjetDaoToObjet(ObjetDao objetDao) {
		Objet objet = new Objet();
		objet.setId_objet(objetDao.getId_objet());
		objet.setNom(objetDao.getNom());
		objet.setRelease(objetDao.getRelease());
		return objet;
	}

	
	
	
	

}
