package fr.d4.demo.dto;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.d4.demo.IDto.IDtoGeneral;
import fr.d4.demo.IDto.IDtoObjet;
import fr.d4.demo.entites.Objet;
import fr.d4.demo.interfaces.IObjetRepository;
import fr.d4.demo.model.ObjetDao;
@Service
public class DtoObjet implements IDtoObjet {
	
	@Autowired
	private IObjetRepository objetRepository;
	
	@Autowired
	private IDtoGeneral dtoGeneral;


	@Override
	public Map<Long, Objet> allObjet() {
		Map<Long, Objet> listeObjet = new HashMap<Long, Objet>();
		for (ObjetDao oeuvreDao : objetRepository.findAll()) {
			listeObjet.put(oeuvreDao.getId_objet(), dtoGeneral.ObjetDaoToObjet(oeuvreDao));
		}
		return listeObjet;
	}

}
