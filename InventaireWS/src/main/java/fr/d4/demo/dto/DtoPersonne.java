package fr.d4.demo.dto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.d4.demo.IDto.IDtoGeneral;
import fr.d4.demo.IDto.IDtoPersonne;
import fr.d4.demo.entites.Log;
import fr.d4.demo.entites.Personne;
import fr.d4.demo.interfaces.ILogRepository;
import fr.d4.demo.interfaces.IPersonneRepository;
import fr.d4.demo.model.LogDao;
import fr.d4.demo.model.PersonneDao;


@Service
public class DtoPersonne implements IDtoPersonne {
	
	@Autowired
	private IPersonneRepository personneRepository;
	
	@Autowired
	private ILogRepository logRepository;
	
	@Autowired
	private IDtoGeneral dtoGeneral;

	@Override
	public Map<Long, Personne> allPersonnes() {
		Map<Long, Personne> listePersonnes = new HashMap<Long, Personne>();
		for (PersonneDao personneDao : personneRepository.findAll()) {
			listePersonnes.put(personneDao.getId_personne(), dtoGeneral.PersonneDaoToPersonne(personneDao));
		}
		return listePersonnes;
	}

	@Override
	public boolean ValideAuth(String log, String mdp) {
		List<LogDao> logPass = logRepository.findByLoginAndMotdepasse(log, mdp);
		return !logPass.isEmpty();
	}

	@Override
	public Personne TrouvePersonne(String log, String mdp) {
		PersonneDao personneDao = personneRepository
				.findByLogin(log);
		return dtoGeneral.PersonneDaoToPersonne(personneDao);
	}

	@Override
	public void CreationPersonne(Log log, Personne personne) {
		LogDao logDao = dtoGeneral.LogToLogDao(log);
		PersonneDao personneDao = dtoGeneral.PersonneToPersonneDao(personne);
		logDao.setPerslog(personneDao);
		logRepository.save(logDao);
	}

	public boolean supprimer(Long id) {
		if (personneRepository.findById(id).isPresent()) {
			personneRepository.deleteById(id);
			return true;
		}
		return false;
	}

}
