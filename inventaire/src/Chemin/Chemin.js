import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";

import Authentification from '../Page/Authentification';
import Menu from '../Page/Menu';
import CreationCompte from '../Page/CreationCompte';
import MenuAdmin from '../Page/MenuAdmin';
import ListePersonne from '../Page/ListePersonne';

export default function Chemin() {
    return (
        <Router>
            <div>
                <Switch>
                    <Route path="/listePersonne">
                            <ListePersonne />
                    </Route>
                    <Route path="/menuAdmin">
                            <MenuAdmin />
                    </Route>
                    <Route path="/menu">
                            <Menu />
                    </Route>
                    <Route path="/creationCompte">
                            <CreationCompte />
                    </Route>
                    <Route path="/">
                        <Authentification />
                    </Route>
                </Switch>
            </div>
        </Router>
    );
}