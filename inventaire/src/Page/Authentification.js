import React, { useState, useEffect } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { Col, Row } from 'react-bootstrap';

export default function Authentification() {

    const [login, setLogin] = useState();
    const [mdp, setMDP] = useState();
    const [status, setStatus] = useState(-1);
    const [pret, setPret] = useState(false);

    const handleLogin = (evt) => {
        setLogin(evt.target.value);
    }

    const handleMDP = (evt) => {
        setMDP(evt.target.value);
    }

    const getUser = () => {
        fetch(`${process.env.REACT_APP_API_URL}/Auth`, {
            method : "post", 
            headers : {
                "Content-type" : "application/json; charset=UTF-8",
                Accept: 'application/json'
            },
            body : `{
                "login" : "${ login }",
                "motdepasse" : "${ mdp }"
            }`
        })
            .then(response => response.json())
            .then(json => {
                if (json.token != null) {
                    sessionStorage.setItem("token", json.token);
                }
                setStatus(json); 
            })
            .catch(error => {
                console.log(error)
                
            });
    }

    useEffect(() => {
        const fetchJson = async () => {
            setPret(false);
            await getUser();
            setPret(true);
        }
        fetchJson();
        getUser();
    },[])

    if (status.token != null && status.codeReponse === 1) {
        return (<Redirect to="menuAdmin" />);
    }
    else if (status.token != null && status.codeReponse === 0) {
        return (<Redirect to="menu" />);
    }
    else {
        return (
            <div className="container">
                <br />
                <Row md="8" xs="auto" className="fond justify-content-center align-items-center" id="marge_haut">
                    <Col md="8" xs="auto">
                        <br/>
                        
                        <div className="container">
                            <form className="form-group">
                                <div className="input-group">
                                    <input id="login" className="form-control" type="text" placeholder="Login" onChange={handleLogin} />
                                </div>
                                <br /><br />
                                <div className="input-group">
                                    <input id="mdp" className="form-control" type="password" placeholder="Mot de passe" onChange={handleMDP} />
                                </div>
                                <br /><br />
                                <input className="btn btn-primary" type="button" value="Connexion" onClick={getUser} />
                                <br /><br />
                                <Link to="creationCompte">Création compte utilisateur</Link>
                            </form>
                        </div>
                    </Col>
                </Row>
            </div>
        )
    }

}