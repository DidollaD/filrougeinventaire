import React, { Fragment } from 'react';

import Menu from '../Composant/MenuAdmin';

import RecupPersonne from '../Composant/RecupPersonne';

export default function ListePersonne() {
   
    return <Fragment>
        <Menu />
        <RecupPersonne />
    </Fragment>
   
}