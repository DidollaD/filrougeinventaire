import React, { useState, Fragment } from 'react';
import { Redirect, Link } from 'react-router-dom';
import { Button } from 'react-bootstrap';

export default function CreationCompte() {

    const [nom, setNom] = useState();
    const [email, setEmail] = useState();
    const [login, setLogin] = useState();
    const [mdp, setMDP] = useState();
    const [mdpVerif, setMDPVerif] = useState();
    const [status, setStatus] = useState(-1);

    const handleNom = (evt) => {
        setNom(evt.target.value);
    }

    const handleEmail = (evt) => {
        setEmail(evt.target.value);
    }

    const handleLogin = (evt) => {
        setLogin(evt.target.value);
    }

    const handleMDP = (evt) => {
        setMDP(evt.target.value);
    }

    const handleMDPVerif = (evt) => {
        setMDPVerif(evt.target.value);
    }
    if (status === 0) {
        return (<Redirect to="" />);
    }
    else if (status !== 0) {
        return <Fragment>
            <div  >
                <br /><br />
                <div className="fond">
                    <h1 class="text-center">Création d'un compte</h1>
                    <br />
                    <div className="container">
                        <form action="http://localhost:3000/" className="form-group">
                            <div className="row">
                                <div className="col-sm-6">
                                    <label for="nom">Nom Complet</label>
                                    <input id="nom" className="form-control" type="text" placeholder="" onChange={handleNom} />
                                </div>
                                <br />
                                <div className="col-sm-6">
                                    <label for="email">Email</label>
                                    <input id="email" className="form-control" type="mail" placeholder="" onChange={handleEmail} />
                                </div>
                            </div>
                            <br />
                            <div className="row">
                                <div className="col-sm-6">
                                    <label for="login">Login</label>
                                    <input id="login" className="form-control" type="text" placeholder="" onChange={handleLogin} />
                                </div>
                            </div>
                            <br />
                            <div className="row">
                                <div className="col-sm-6">
                                    <label for="mdp">Mot de passe</label>
                                    <input id="mdp" className="form-control" type="password" placeholder="" onChange={handleMDP} />
                                </div>
                                <br />
                                <div className="col-sm-6">
                                    <label for="mdpVerif">Valider mot de passe</label>
                                    <input id="mdpVerif" className="form-control" type="password" placeholder="" onChange={handleMDPVerif} />
                                </div>
                            </div>
                            <br />
                            <Button className="btn btn-info" onClick={() => creerUtilisateur(nom, email, login, mdp, mdpVerif, setStatus)}>Créer</Button>
                            <span className="container">
                                <Link to=""><input className="btn btn-outline-info" type="button" value="Retour" /></Link>
                            </span>

                        </form>
                        <br />
                    </div>
                </div>
            </div>
    
    </Fragment>
    }
}

function creerUtilisateur(nom, email, login, mdp, mdpVerif, setStatus) {
    fetch(`${process.env.REACT_APP_API_URL}/CUP`, {
        method: "post",
        headers: {
            "Content-type": "application/json; charset=UTF-8",
            Accept: 'application/json'
        },
        body: `{
                "nom": "${ nom}",
                "mail": "${ email}",
                "login": "${ login}",
                "password": "${ mdp}",
                "password2": "${ mdpVerif}",
                "create": "pageUser"
            }`
    })
        .then(response => response.json())
        .then(json => {
            setStatus(json.codeRetour);
        })
        .catch(error => {
            console.log(error);
            setStatus(-1);
        })
}