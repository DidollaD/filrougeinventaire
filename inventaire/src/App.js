import React from 'react';
import Chemin from './Chemin/Chemin';
import './App.css';

function App() {
  return (
    <div className="App">
      <Chemin />
    </div>
  );
}

export default App;
