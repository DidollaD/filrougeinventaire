import React, { useState, useEffect, Fragment } from 'react';

export default function RecupOeuvre(props) {
    const [pret, setPret] = useState(false);
    const [liste, setListe] = useState([]);
    const [codeRole, setCodeRole] = useState()

    useEffect(() => {
        const fetchJson = async () => {
            setPret(false);
            await recup(setListe, setCodeRole);
            setPret(true);
        }
        fetchJson();
        recup(setListe, setCodeRole);
    }, []);

    return <Fragment>
        { 
        (pret && codeRole != 200) ? (<Redirect to="" />) :
        (pret ? (
            <div className="Container">
            <div className="">
                <table className=" ">
                    <thead className="">
                        <tr>
                            <th>Nom</th>
                            <th>Sortie</th>
                            <th>Type</th>
                            <th>Créateur</th>
                        </tr>
                    </thead>
                    {recupListe(liste)}
                </table>
                </div>
            </div>
        ) : (<div></div>))}
    </Fragment>

    function recupListe(liste) {
        return (
            <tbody>
            {liste.map((oeuvre) =>
                    <tr key={oeuvre.id_personne}>
                        <td>{oeuvre.nom}</td>
                        <td>{oeuvre.realese}</td>
                    </tr>
                )}
            </tbody>
        );
    }
}
async function recup(setListe, setCodeRole) {
    await fetch(`${ process.env.REACT_APP_API_URL }/SVURest`, {
         method : "get",
         headers : {
            "Authorization": sessionStorage.token
        }
    })
    .then(response => response.json())
    .then(json => { 
        setListe(json.objetRetourne);
        setCodeRole(json.codeRole);
    })
    .catch(error => { console.log(error); })
    }
}