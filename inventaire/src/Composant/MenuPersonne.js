import React from 'react';
import { Link } from 'react-router-dom';

export default function MenuPersonnes() {
    return (
        <header>
            <ul className="nav nav-tabs  ">
                
                <li className="nav-item ">
                    <Link className="nav-link" to="/">Inventaire</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/">Recherche</Link>
                </li>
                <li className="nav-item">
                    <Link className="nav-link" to="/">Page Personel</Link>
                </li>
                <li className="nav-item ml-auto">
                    <Link className="nav-link deconnexion" to="/">Déconnexion</Link>
                </li>
            </ul>
        </header>
    );
}