import React, { useState, useEffect, Fragment } from 'react';

export default function RecupPersonnes(props) {
    const [liste, setListe] = useState([]);

    useEffect(() => {
        const fetchJson = async () => {
            await recup(setListe);
        }
        fetchJson();
        recup(setListe);
    }, []);

    return <Fragment>
        <div className="Container">
            <div className="row col-md-12 col-md-offset-2 ">
                <table className=" table table-striped fond">
                    <thead className="thead-dark">
                        <tr>
                            <th>Login</th>
                            <th>Nom</th>
                            <th>Mail</th>
                        </tr>
                    </thead>
                    {recupListe(liste)}
                </table>
                </div>
        </div>
    </Fragment>
}

function recupListe(liste) {
    return (
        <tbody>
            {liste.map((personne) =>
                <tr key={personne.id_personne}>
                    <td>{personne.login}</td>
                    <td>{personne.nomComplet}</td>
                    <td>{personne.email}</td>
                </tr>
            )}
        </tbody>
    );
}

async function recup(setListe) {
    await fetch(`${ process.env.REACT_APP_API_URL }/VLU`, {
        method : "get",
        headers : {
            "Authorization": sessionStorage.token
        }
    })
    .then(response => response.json())
    .then(json => { 
        setListe(json.objetRetourne);
    })
    .catch(error => { console.log(error); })

}